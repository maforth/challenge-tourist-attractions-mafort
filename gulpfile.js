const { src,dest,watch } = require('gulp');
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

function styles(){
    return src("src/styles/main.scss").pipe(sass().on('error', sass.logError)).pipe(gulp.dest('dist'));
}
function sentinel() {
  watch('src/styles/**/*.scss',styles);
}
exports.styles = sentinel;



  
